class StudentScoresController < ApplicationController

  def index
    @student_scores = StudentScores.all
  end

  def show
  end

  def new
    @student_scores = StudentScores.new
  end

  def create
    @student_scores = StudentScores.new(student_score_params)

    if @student_scores.save
      redirect_to @student_scores, notice: '作成しました'
    else
      render :new
    end
  end

  private

    def student_score_params
      params.require(:student_score).permit(:student_id, :subject_id, :mid_exam_score, :finel_exam_score, :total_assessment, :resit_score, :resit_assessment)
    end
end

