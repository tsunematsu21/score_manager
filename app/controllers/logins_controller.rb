class LoginsController < ApplicationController
  layout 'login'

  def show
    render "new"
  end

  def create
    teacher = Teacher.find_by login_id: params[:login_id]
    if teacher && teacher.authenticate(params[:password])
      session[:teacher_id] = teacher.id
      flash[:toast] = 'ログインしました'
      redirect_to root_path
    else
      flash.now.alert = "失敗しました。login_id=#{params[:login_id]}"
      render "new"
    end
  end

  def destroy
    reset_session
    flash[:toast] = 'ログアウトしました'
    redirect_to login_path
  end

  private

  def use_authenticate?
    false
  end
end