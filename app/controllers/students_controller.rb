class StudentsController < ApplicationController
  def index
    @students = Student.all
  end

  def show
    @student = Student.find(params[:id])
  end

  def new
    @student = Student.new
  end

  def create
    @student = Student.new(student_params)

    if @student.save
      # クラスのページから生徒を作成した場合
      if params[:school_class_id]
        student_class = @student.student_classes.build do |s|
          s.school_class_id = params[:school_class_id]
        end
        if student_class.save
          flash.now[:notice] = "生徒をクラスに追加しました"
        else
          flash.now[:alert]  = "生徒の追加に失敗しました"
        end
        redirect_to "/school_classes/#{params[:school_class_id]}"
      else
        redirect_to @student, notice: '作成しました'
      end
    else
      render :new
    end
  end

  private

    def student_params
      params.require(:student).permit(:name, :kana, :student_number)
    end
end

