class SchoolClassesController < ApplicationController
  before_action :authenticate
  before_action :set_school_class, only: [:show, :edit, :update, :destroy]
  before_action :set_school_class_masters, only: [:new, :edit]
  before_action :authenticate_teacher, only: [:edit, :update, :destroy]

  def index
    @school_classes = SchoolClass.all
  end

  def show
    @teachers = Teacher.where.not('id = ?', current_teacher.id)
    @student  = Student.new
  end

  def new
    @school_class = SchoolClass.new
  end

  def create
    @school_class = SchoolClass.new(school_class_params)

    # クラスを作成した教員が担任に
    if @school_class.save
      class_teacher = current_teacher.class_teachers.build do |ct|
        ct.school_class_id = @school_class.id
      end
      class_teacher.save
      redirect_to @school_class, notice: '作成しました'
    else
      render :new
    end
  end

  private

  def set_school_class
    @school_class = SchoolClass.find(params[:id])
  end

  def set_school_class_masters
    @school_class_masters = SchoolClassMaster.all
  end

  def school_class_params
    params.require(:school_class).permit(:name, :grade, :year,:course)
  end

  def authenticate_teacher
    redirect_to :back, alert: '許可されていません' unless SchoolClass.find(params[:id]).teachers.include? current_teacher
  end
end