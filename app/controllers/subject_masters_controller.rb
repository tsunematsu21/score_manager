class SubjectMastersController < InheritedResources::Base

  private

    def subject_master_params
      params.require(:subject_master).permit(:name, :credit, :time)
    end
end

