class SubjectsController < ApplicationController
  before_action :set_subject, only: [:show, :edit, :update, :destroy]
  before_action :set_subject_masters, only: [:new, :edit, :update]

  def index
    @q        = Subject.search(search_params)
    @subjects = @q.result(distinct: true)
  end

  def show
  end

  def new
    # @subject = Subject.new
    if params[:subject_master_id]
      @subject = Subject.new
      @subject_master = SubjectMaster.find(params[:subject_master_id])
    else
      @q               = SubjectMaster.search(search_params)
      @subject_masters = @q.result(distinct: true)
    end
  end

  def create
    @subject = Subject.new(subject_params)

    if @subject.save
      teacher_subject = current_teacher.teacher_subjects.build do |ts|
        ts.subject_id = @subject.id
      end
      teacher_subject.save
      redirect_to @subject, notice: '作成しました'
    else
      render :new
    end
  end

  def edit
  end

  def destroy
  end

  private

  def set_subject
    @subject = Subject.find(params[:id])
  end

  def set_subject_masters
    @subject_masters = SubjectMaster.all
  end

  def subject_params
    params.require(:subject).permit(:subject_type, :year, :semester, :subject_master_id)
  end

  def search_params
    params.require(:q).permit(
      :s,
      :subject_master_name_cont,
      :semester_eq,
      :year_eq,
      :subject_type_eq,

      :name_cont
    )
  rescue
    nil
  end
end

