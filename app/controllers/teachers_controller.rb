class TeachersController < ApplicationController
  before_action :set_teachers, only: [:show, :edit, :update, :destroy]

  def index
    @teachers = Teacher.all
  end

  def show
	@classes = Teacher.find(params[:id]).school_classes
	@subjects = Teacher.find(params[:id]).subjects
  end

  private
  def set_teachers
    @teacher = Teacher.find(params[:id])
  end

  def user_params
    params.require(:teacher).permit(:title, :body)
  end
end
