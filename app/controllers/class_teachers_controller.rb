class ClassTeachersController < ApplicationController
  def new
  end

  def create
    school_class = SchoolClass.find(params[:school_class_id])

    class_teacher = school_class.class_teachers.build do |ct|
      ct.teacher_id = params[:class_teacher][:teacher_id]
    end

    if class_teacher.save
      redirect_to school_class_path(params[:school_class_id]), notice: '追加しました'
    else
      redirect_to school_class_path(params[:school_class_id]), alert: '追加失敗'
    end
  end

  def destroy
    ticket = current_user.tickets.find_by!(event_id: params[:event_id])
    ticket.destroy!
    redirect_to event_path(params[:event_id]), notice: 'このイベントの参加をキャンセルしました'
  end
end
