class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate, if: :use_authenticate?

  helper_method :current_teacher, :logged_in?

  private
  # ログインしているユーザを取得
  def current_teacher
    return unless session[:teacher_id] #ログインしていなければ
    @current_teacher ||= Teacher.find(session[:teacher_id])
  end

  # ログインしているかどうか
  def logged_in?
    !!session[:teacher_id]
  end

  # ログインしていなければloginページにリダイレクト
  def authenticate
    return if logged_in?
    redirect_to login_path, alert: 'ログインしてください'
  end

  # ユーザーの現在のURLを保存
  def store_location
    session[:return_to] = request.fullpath
  end

  def use_authenticate?
    true
  end
end
