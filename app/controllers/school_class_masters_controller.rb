class SchoolClassMastersController < InheritedResources::Base

  private

    def school_class_master_params
      params.require(:school_class_master).permit(:name, :enough_credits, :enough_times, :graduation_grade)
    end
end

