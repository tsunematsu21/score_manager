class StudentClassesController < ApplicationController

  def new
    @school_class = SchoolClass.find params[:school_class_id]
    @students = Student.where.not(id: StudentClass.select(:student_id))
  end

  def create
    params[:student_ids].each do |sid|
      student_class = StudentClass.new
      student_class.student_id      = sid
      student_class.school_class_id = params[:school_class_id]
      student_class.save
    end

    redirect_to school_class_path(params[:school_class_id])
  end

  private

    def student_class_params
      params.require(:student_class).permit(:student_id, :school_class_id, :attendance_number)
    end
end

