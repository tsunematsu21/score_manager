ActiveAdmin.register Teacher do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  permit_params :login_id, :password

  index do
    column :name
    column :login_id
    column :password
  end

  form do |f|
    f.inputs "Teacher Details" do
      f.input :name
      f.input :login_id
      f.input :password
    end
    f.actions
  end
end