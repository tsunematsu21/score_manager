json.array!(@subject_masters) do |subject_master|
  json.extract! subject_master, :id, :name, :credit, :time
  json.url subject_master_url(subject_master, format: :json)
end
