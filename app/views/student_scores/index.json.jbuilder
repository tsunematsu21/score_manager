json.array!(@student_scores) do |student_score|
  json.extract! student_score, :id, :student_id, :subject_id, :mid_exam_score, :finel_exam_score, :total_assessment, :resit_score, :resit_assessment
  json.url student_score_url(student_score, format: :json)
end
