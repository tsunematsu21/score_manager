json.array!(@student_classes) do |student_class|
  json.extract! student_class, :id, :student_id, :school_class_id, :attendance_number
  json.url student_class_url(student_class, format: :json)
end
