json.array!(@subjects) do |subject|
  json.extract! subject, :id, :type, :year, :semester
  json.url subject_url(subject, format: :json)
end
