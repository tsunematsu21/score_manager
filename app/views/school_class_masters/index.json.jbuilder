json.array!(@school_class_masters) do |school_class_master|
  json.extract! school_class_master, :id, :name, :enough_credits, :enough_times, :graduation_grade
  json.url school_class_master_url(school_class_master, format: :json)
end
