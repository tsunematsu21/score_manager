class SchoolClass < ActiveRecord::Base
  has_many :class_teachers, dependent: :destroy
  has_many :teachers, through: :class_teachers

  has_many :student_classes, dependent: :destroy
  has_many :students, through: :student_classes

  has_many :school_class_subjects, dependent: :destroy
  has_many :subjects, through: :school_class_subjects

  belongs_to :school_class_master
end
