class Student < ActiveRecord::Base
  has_many :student_classes, dependent: :destroy
  has_many :school_classes, through: :student_classes

  has_many :student_scores, dependent: :destroy
  has_many :subjects, through: :student_scores

  attr_accessor :school_class_id
end
