class Teacher < ActiveRecord::Base
  has_many :class_teachers
  has_many :school_classes, through: :class_teachers
  
  has_many :teacher_subjects, dependent: :destroy
  has_many :subjects, through: :teacher_subjects

  before_save :encryption_to_password

  def authenticate password
    self.password == Digest::MD5.new.update(password).to_s
  end

  private

  def encryption_to_password
    self.password = Digest::MD5.new.update(self.password).to_s
    self
  end
end