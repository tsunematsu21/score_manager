class Subject < ActiveRecord::Base
  has_many :student_scores, dependent: :destroy
  has_many :students, through: :student_scores

  has_many :teacher_subjects, dependent: :destroy
  has_many :teachers, through: :teacher_subjects

  has_many :school_class_subjects, dependent: :destroy
  has_many :school_classes, through: :school_class_subjects

  belongs_to :subject_master

  def name
    self.subject_master.name
  end

  def semester_to_string
    case self.semester
    when 1
      '前期'
    when 2
      '後期'
    end
  end
end
