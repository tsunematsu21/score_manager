# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150609050345) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "class_teachers", force: :cascade do |t|
    t.integer  "school_class_id", null: false
    t.integer  "teacher_id",      null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "school_class_masters", force: :cascade do |t|
    t.text     "name",             null: false
    t.integer  "enough_credits",   null: false
    t.integer  "enough_times",     null: false
    t.integer  "graduation_grade", null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "school_class_subjects", force: :cascade do |t|
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "school_class_id"
    t.integer  "subject_id"
  end

  create_table "school_classes", force: :cascade do |t|
    t.string   "name",                   null: false
    t.integer  "grade",                  null: false
    t.integer  "year",                   null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "school_class_master_id"
  end

  create_table "student_classes", force: :cascade do |t|
    t.integer  "student_id"
    t.integer  "school_class_id"
    t.integer  "attendance_number"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "student_scores", force: :cascade do |t|
    t.integer  "student_id",       null: false
    t.integer  "subject_id",       null: false
    t.integer  "mid_exam_score"
    t.integer  "finel_exam_score"
    t.integer  "total_assessment"
    t.integer  "resit_score"
    t.integer  "resit_assessment"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "students", force: :cascade do |t|
    t.string   "name"
    t.string   "student_number"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "kana"
  end

  create_table "subject_masters", force: :cascade do |t|
    t.text     "name",       null: false
    t.integer  "credit",     null: false
    t.integer  "time",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subjects", force: :cascade do |t|
    t.string   "subject_type",      null: false
    t.integer  "year",              null: false
    t.integer  "semester",          null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "subject_master_id"
  end

  create_table "teacher_subjects", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "teacher_id"
    t.integer  "subject_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.string   "login_id",                null: false
    t.string   "password",                null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "name",       default: ""
  end

end
