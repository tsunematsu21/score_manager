# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#############################
# 初期データ
#############################

Teacher.first_or_create([
  {id: 1, login_id: 'hoge', password: 'password', name: '山田太郎'},
  {id: 2, login_id: 'huge', password: 'password', name: '山田花子'},
  {id: 3, login_id: 'hage', password: 'password', name: 'ジョン万次郎'},
])

Student.first_or_create([
  {id: 1, name: '春野はるか', kana: 'はるのはるか', student_number: 'abcd0001'},
  {id: 2, name: '海藤みなみ', kana: 'かいどうみなみ', student_number: 'abcd0002'},
  {id: 3, name: '天ノ川きらら', kana: 'あまのがわきらら', student_number: 'abcd0003'},
  {id: 4, name: '範馬刃牙', kana: 'はんまばき', student_number: 'abcd0004'},
  {id: 5, name: '大豪院邪鬼', kana: 'だいごういんじゃき', student_number: 'abcd0005'},
])

SubjectMaster.first_or_create([
  {id: 1, name: '数学', credit: 1, time: 100},
  {id: 2, name: '英語', credit: 2, time: 200},
  {id: 3, name: '国語', credit: 3, time: 300},
])

Subject.first_or_create([
	{id: 1,subject_type:'必修',year:2000,semester:1,subject_master_id:1},
	{id: 2,subject_type:'必修',year:2000,semester:1,subject_master_id:2},
	{id: 3,subject_type:'必修',year:2000,semester:1,subject_master_id:3},
	{id: 4,subject_type:'選択',year:2000,semester:1,subject_master_id:3},
])

SchoolClassMaster.first_or_create([
	{id:1,name:'高度情報システム科',enough_credits:400,enough_times:4000,graduation_grade:4},
	{id:2,name:'情報処理化３年制',enough_credits:200,enough_times:2000,graduation_grade:3},
])

SchoolClass.first_or_create([
	{id:1,name:'J4',grade:4,year:2000,school_class_master_id:1},
	{id:2,name:'J3',grade:3,year:2000,school_class_master_id:1},
	{id:3,name:'A3',grade:3,year:2000,school_class_master_id:2},
])

ClassTeacher.first_or_create([
	{id:1,school_class_id:1,teacher_id:1},
	{id:2,school_class_id:2,teacher_id:2},
	{id:3,school_class_id:1,teacher_id:3},
	{id:4,school_class_id:3,teacher_id:3},
])

SchoolClassSubject.first_or_create([
	{id:1,school_class_id:1,subject_id:1},
	{id:2,school_class_id:2,subject_id:2},
	{id:3,school_class_id:3,subject_id:3},
	{id:4,school_class_id:2,subject_id:4},
])

StudentClass.first_or_create([
	{id:1,student_id:1,school_class_id:1,attendance_number:1},
	{id:2,student_id:2,school_class_id:1,attendance_number:2},
	{id:3,student_id:3,school_class_id:2,attendance_number:1},
	{id:4,student_id:4,school_class_id:2,attendance_number:2},
	{id:5,student_id:5,school_class_id:3,attendance_number:1},
])

StudentScore.first_or_create([
	{id:1,subject_id:1,student_id:1},
	{id:2,subject_id:1,student_id:2},
	{id:3,subject_id:2,student_id:3},
	{id:4,subject_id:2,student_id:4},
	{id:6,subject_id:3,student_id:5},
	{id:5,subject_id:4,student_id:3},
])

TeacherSubject.first_or_create([
	{id:1,subject_id:1,teacher_id:1},
	{id:2,subject_id:2,teacher_id:2},
	{id:3,subject_id:3,teacher_id:3},
	{id:4,subject_id:4,teacher_id:3},
])
