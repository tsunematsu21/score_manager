class CreateStudentScores < ActiveRecord::Migration
  def change
    create_table :student_scores do |t|
	  t.integer   :student_id,null:false
	  t.integer   :subject_id,null:false
	  
	  t.integer   :mid_exam_score
	  t.integer   :finel_exam_score
	  t.integer   :total_assessment
	  t.integer   :resit_score
	  t.integer   :resit_assessment
	
      t.timestamps null: false
    end
  end
end
