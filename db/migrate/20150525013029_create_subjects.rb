class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
	  t.string :type,null: false
	  t.integer :year,null: false
	  t.integer :semester,null: false 
      t.timestamps null: false
    end
  end
end
