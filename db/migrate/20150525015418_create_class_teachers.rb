class CreateClassTeachers < ActiveRecord::Migration
  def change
    create_table :class_teachers do |t|
      t.references :school_class,null: false
      t.references :teacher,null: false

      t.timestamps null: false
    end
  end
end
