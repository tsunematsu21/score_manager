class RenameColumnToUsers < ActiveRecord::Migration
  def change
    rename_column :users, :user_id, :login_id
  end
end
