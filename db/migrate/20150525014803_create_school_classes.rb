class CreateSchoolClasses < ActiveRecord::Migration
  def change
    create_table :school_classes do |t|
      t.string :name,null: false
      t.integer :grade,null: false
      t.integer :year,null: false

      t.timestamps null: false
    end
  end
end
