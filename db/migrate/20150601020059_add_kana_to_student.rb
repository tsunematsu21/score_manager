class AddKanaToStudent < ActiveRecord::Migration
  def change
    add_column :students, :kana, :string
  end
end
