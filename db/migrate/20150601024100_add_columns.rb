class AddColumns < ActiveRecord::Migration
  def change
    add_column :school_class_subjects, :school_class_id, :integer
    add_column :school_class_subjects, :subject_id, :integer
    add_column :teacher_subjects, :teacher_id, :integer
    add_column :teacher_subjects, :subject_id, :integer
  end
end
