class CreateSubjectMasters < ActiveRecord::Migration
  def change
    create_table :subject_masters do |t|
	  t.text	:name,null:false
	  t.integer :credit,null:false
	  t.integer :time,null:false

      t.timestamps null: false
    end
  end
end
