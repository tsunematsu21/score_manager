class CreateSchoolClassSubjects < ActiveRecord::Migration
  def change
    create_table :school_class_subjects do |t|

      t.timestamps null: false
    end
  end
end
