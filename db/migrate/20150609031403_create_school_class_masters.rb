class CreateSchoolClassMasters < ActiveRecord::Migration
  def change
    create_table :school_class_masters do |t|
	  t.text	:name,null:false
	  t.integer :enough_credits,null:false
	  t.integer :enough_times,null:false
	  t.integer :graduation_grade,null:false
	  
      t.timestamps null: false
    end
  end
end
