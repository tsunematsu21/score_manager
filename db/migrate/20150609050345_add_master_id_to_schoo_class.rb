class AddMasterIdToSchooClass < ActiveRecord::Migration
  def change
    add_column :school_classes, :school_class_master_id, :integer
  end
end
