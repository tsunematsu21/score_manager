class RemoveCourseFromSchoolClass < ActiveRecord::Migration
  def change
    remove_column :school_classes, :course, :string
  end
end
