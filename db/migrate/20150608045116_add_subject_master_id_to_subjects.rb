class AddSubjectMasterIdToSubjects < ActiveRecord::Migration
  def change
    add_column :subjects, :subject_master_id, :integer
  end
end
