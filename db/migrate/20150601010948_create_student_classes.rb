class CreateStudentClasses < ActiveRecord::Migration
  def change
    create_table :student_classes do |t|
      t.integer :student_id
      t.integer :school_class_id
	  t.integer :attendance_number

      t.timestamps null: false
    end
  end
end
