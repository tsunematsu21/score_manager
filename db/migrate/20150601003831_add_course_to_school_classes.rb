class AddCourseToSchoolClasses < ActiveRecord::Migration
  def change
    add_column :school_classes, :course, :string
  end
end
